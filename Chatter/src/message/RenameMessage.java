package message;

public class RenameMessage extends TextMessage{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 107L;
	private String oldName;
	private String newName;
	
	public RenameMessage(String oldName, String newName) {
		this.oldName = oldName;
		this.newName = newName;
	}

	@Override
	public Type getType() {
		return Type.RENAME;
	}
	
	@Override
	public String getMessageText() {
		return oldName + " has become " + newName;
	}
	
	public String getOldName() {
		return oldName;
	}
	
	public String getNewName() {
		return newName;
	}
}
