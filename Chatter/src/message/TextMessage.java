package message;

public abstract class TextMessage extends Message{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 101L;

	@Override
	public boolean isTextMessage() {
		return true;
	}
	
	public abstract String getMessageText();
}
