package message;

public class BroadcastMessage extends TextMessage{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 102L;
	String message;

	public BroadcastMessage(String message) {
		this.message = message;
	}

	@Override
	public Type getType() {
		return Type.BROADCAST;
	}
	
	@Override
	public String getMessageText() {
		return message;
	}
	
	public String getMessage() {
		return message;
	}
}
