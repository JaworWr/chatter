package message;

public class JoinLeaveMessage extends TextMessage{

	/**
	 * 
	 */
	private static final long serialVersionUID = 105L;

	public static enum Reason {
		JOINED(0), LEFT(1), LOST(2), KICKED(3), UNDEFINED(-1);
		
		private int value;
		private Reason(int value) {
			this.value = value;
		}
		
		static Reason fromId(byte id) {
			switch(id) {
			case 0:
				return JOINED;
			case 1:
				return LEFT;
			case 2:
				return LOST;
			case 3:
				return KICKED;
			default:
				return UNDEFINED;
			}
		}
		
		public byte getId() {
			return (byte)value;
		}
	}
	
	private String name;
	private Reason reason;
	private String reasonText;
	
	public JoinLeaveMessage(String name, Reason reason) {
		this.name = name;
		this.reason = reason;
		this.reasonText = null;
	}
	
	public JoinLeaveMessage(String name, Reason reason, String reasonText) {
		this.name = name;
		this.reason = reason;
		this.reasonText = reasonText;
	}
	
	JoinLeaveMessage(String name, byte reasonId) {
		this.name = name;
		this.reason = Reason.fromId(reasonId);
		this.reasonText = null;
	}
	
	JoinLeaveMessage(String name, byte reasonId, String reasonText) {
		this.name = name;
		this.reason = Reason.fromId(reasonId);
		this.reasonText = reasonText;
	}

	@Override
	public Type getType() {
		return Type.JOIN_LEAVE;
	}
	
	@Override
	public String getMessageText() {
		String str = name;
		switch(reason) {
		case JOINED:
			str += " has joined the server.";
			break;
		case LEFT:
			str += " has left the server.";
			break;
		case LOST:
			str += " has lost connection to the server.";
			break;
		case KICKED:
			str += " has been kicked." + 
				(reasonText == null ? "" : " Reason: " + reasonText);
			break;
		default:
			str = "undefined";
		}
		return str;
	}

	public String getName() {
		return name;
	}

	public Reason getReason() {
		return reason;
	}

	public String getReasonText() {
		return reasonText;
	}
	
}
