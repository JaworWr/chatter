package message;

public class UserListMessage extends Message {

	/**
	 * 
	 */
	private static final long serialVersionUID = 110L;
	private String[] list;
	private byte count;
	
	public static final int MAX_COUNT = 8;
	
	public UserListMessage(byte count, String[] list) {
		this.count = count;
		this.list = list;
	}
	
	@Override
	public Type getType() {
		return Type.USER_LIST;
	}

	public String[] getList() {
		return list;
	}

	public byte getCount() {
		return count;
	}
	
}
