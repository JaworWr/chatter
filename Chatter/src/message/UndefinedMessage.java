package message;

public class UndefinedMessage extends TextMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 109L;

	@Override
	public String getMessageText() {
		return "<undefined>";
	}

	@Override
	public Type getType() {
		return Type.UNDEFINED;
	}

}
