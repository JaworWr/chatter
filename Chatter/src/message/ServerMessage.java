package message;

public class ServerMessage extends Message{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 108L;

	public static enum Statement {
		KICKED, CLOSED, DUPLICATE, UNDEFINED		

	}
	
	private Statement statement;
	private String message;
	
	public ServerMessage(Statement statement) {
		this.statement = statement;
	}
	
	public ServerMessage(Statement statement, String message) {
		this.statement = statement;
		this.message = message;
	}

	@Override
	public Type getType() {
		return Type.SERVER;
	}
	
	public Statement getStatement() {
		return statement;
	}
	
	public String getMessage() {
		return message;
	}
}
