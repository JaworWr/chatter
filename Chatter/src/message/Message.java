package message;
import java.io.Serializable;

public abstract class Message implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 100L;

	public static enum Type {
		CHAT, SERVER, RENAME, BROADCAST, JOIN_LEAVE, 
		GREETING, USER_LIST, UNDEFINED
	}
	
	abstract public Type getType();
	public boolean isTextMessage(){
		return false;
	}
	
}
