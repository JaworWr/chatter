package message;

public class ChatMessage extends TextMessage{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 103L;

	private String name;

	private String message;
	
	public ChatMessage(String name, String message) {
		this.name = name;
		this.message = message;
	}

	@Override
	public Type getType() {
		return Type.CHAT;
	}
	
	@Override
	public String getMessageText() {
		return name + ": " + message;
	}
	
	public String getName() {
		return name;
	}

	public String getMessage() {
		return message;
	}
}
