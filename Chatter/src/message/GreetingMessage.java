package message;

public class GreetingMessage extends Message {

	/**
	 * 
	 */
	private static final long serialVersionUID = 104L;
	private byte count;
	private byte maxUsers;
	private String name;
	
	public GreetingMessage() {
		this.count = (byte)-1;
	}
	
	public GreetingMessage(byte count, byte maxUsers, String name) {
		this.count = count;
		this.maxUsers = maxUsers;
		this.name = name;
	}
	
	public GreetingMessage(String name) {
		this.count = 0;
		this.name = name;
	}
	
	@Override
	public Type getType() {
		return Type.GREETING;
	}

	public byte getCount() {
		return count;
	}
	
	public byte getMaxUsers() {
		return maxUsers;
	}

	public String getName() {
		return name;
	}
	
}
