package chatter;

public class ServerInfo {
	private String address;
	private int maxUsers;
	private String username;
	private int port;

	public ServerInfo(String address, String username, int port) {
		this.address = address;
		this.username = username;
		this.port = port;
	}

	public ServerInfo(int maxUsers, String username, int port) {
		this.maxUsers = maxUsers;
		this.username = username;
		this.port = port;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getMaxUsers() {
		return maxUsers;
	}

	public void setMaxUsers(int maxUsers) {
		this.maxUsers = maxUsers;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}
	
}
