package chatter;
import java.util.Optional;

import chatterSocket.ChatterClientSocket;
import chatterSocket.ChatterServer;
import chatterSocket.ChatterServerOutputHandler;
import chatterSocket.ChatterSocketInputHandler;
import chatterSocket.ChatterSocketOutputHandler;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.StringBinding;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;
import javafx.stage.Stage;
import javafx.util.Callback;
import message.TextMessage;

public class Chatter extends Application implements ChatterSocketInputHandler{
	/* TODO:
	 *  thread pool instead of actual threads
	 *  chat commands
	 *  whispers
	 */
	
	public static final int MAX_MES_LEN = 200;
	public static final int MAX_NAME_LEN = 30;
	
	private ChatOutputArea chat;
	private ListView<String> userList;
	private BorderPane outerPane;
	private IntegerProperty userCount;
	private IntegerProperty userMax;
	
	private GridPane homePane;
	private GridPane conPane;
	
	private MenuBar homeMenu;
	private MenuBar clientMenu;
	private MenuBar serverMenu;
	
	private ChatterSocketOutputHandler handler;
	private ChatterServerOutputHandler sHandler;
	
	private void addUser(String username) {
		userList.getItems().add(username);
		userCount.set(userCount.get() + 1);
	}
	
	private void removeUser(String username) {
		userList.getItems().remove(username);
		userCount.set(userCount.get() - 1);
	}
	
	private void tryClose() {
		if(handler != null)
			handler.stop();
	}
	
	private void switchPane(Pane pane) {
		outerPane.setCenter(pane);
	}
	
	private void switchMenu(MenuBar menu) {
		outerPane.setTop(menu);
	}
	
	private void switchCellFactory(Callback<ListView<String>, ListCell<String>> f) {
		userList.setCellFactory(f);
	}
	
	private void resetView() {
		switchPane(homePane);
		switchMenu(homeMenu);
		handler = null;
		sHandler = null;
		userList.getItems().clear();
		userCount.set(0);
		chat.clear();
	}
	
	private void clientView(String username) {
		switchPane(conPane);
		switchMenu(clientMenu);
		switchCellFactory(lv -> {
			ListCell<String> cell = new ListCell<>();
			
			StringBinding text = Bindings.when(cell.itemProperty().isNull())
					.then("")
					.otherwise(cell.itemProperty().asString());
			
			cell.textProperty().bind(Bindings.when(cell.indexProperty().isEqualTo(0))
					.then(Bindings.concat(text, " (host)"))
					.otherwise(Bindings.when(text.isEqualTo(username))
							.then(Bindings.concat(text, " (you)"))
							.otherwise(text)));
			return cell;
		});
	}
	
	private void serverView() {
		switchPane(conPane);
		switchMenu(serverMenu);
		switchCellFactory(lv -> {
			ListCell<String> cell = new ListCell<>();
			
			ContextMenu menu = new ContextMenu();
			
			MenuItem kickItem = new MenuItem("_Kick");
			kickItem.setOnAction(new EventHandler<ActionEvent>() {
				
				@Override
				public void handle(ActionEvent event) {
					sHandler.kick(cell.getItem(), null);
					
				}
			});
			menu.getItems().add(kickItem);
			
			StringBinding text = Bindings.when(cell.itemProperty().isNull())
					.then("")
					.otherwise(cell.itemProperty().asString());
			
			cell.textProperty().bind(Bindings.when(cell.indexProperty().isEqualTo(0))
					.then(Bindings.concat(text, " (you)"))
					.otherwise(text));
			
			cell.contextMenuProperty().bind(
					Bindings.when(Bindings.or(cell.emptyProperty(), cell.indexProperty().isEqualTo(0)))
					.then((ContextMenu)null)
					.otherwise(menu));
			
			return cell;
		});
	}

	@Override
	public void start(Stage primaryStage) {
		primaryStage.setTitle("Chatter");
		
		outerPane = new BorderPane();
		
		EventHandler<ActionEvent> stopAction = new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				Platform.runLater(() -> {
					handler.stop();
					handler = null;
					sHandler = null;
					resetView();					
				});
			}
		};
		
		EventHandler<ActionEvent> closeAction = new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				Platform.runLater(() -> {
					tryClose();
					primaryStage.close();
				});
			}
		};
		
		homeMenu = new MenuBar();
		
		Menu fileMenu = new Menu("_File");
		homeMenu.getMenus().add(fileMenu);
		
		MenuItem closeItem = new MenuItem("_Close");
		closeItem.setOnAction(closeAction);
		
		fileMenu.getItems().add(closeItem);
	
		clientMenu = new MenuBar();
		
		Menu clientFileMenu = new Menu("_File");
		
		MenuItem disconnectItem = new MenuItem("_Disconnect");
		disconnectItem.setOnAction(stopAction);
		
		closeItem = new MenuItem("_Close");
		closeItem.setOnAction(closeAction);
		
		clientFileMenu.getItems().addAll(disconnectItem, new SeparatorMenuItem(), closeItem);
		
		clientMenu.getMenus().add(clientFileMenu);
		
		serverMenu = new MenuBar();
		
		Menu serverFileMenu = new Menu("_File");
		
		MenuItem stopItem = new MenuItem("_Stop server");
		stopItem.setOnAction(stopAction);
		
		closeItem = new MenuItem("_Close");
		closeItem.setOnAction(closeAction);
		
		serverFileMenu.getItems().addAll(stopItem, new SeparatorMenuItem(), closeItem);
		
		Menu serverServerMenu = new Menu("_Server");
		
		MenuItem broadcastItem = new MenuItem("_Broadcast");
		broadcastItem.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				TextInputDialog dialog = new TextInputDialog();
				dialog.getEditor().setPrefColumnCount(30);
				dialog.getEditor().textProperty().addListener(new ChangeListener<String>() {

					@Override
					public void changed(ObservableValue<? extends String> observable, String oldValue,
							String newValue) {
						if(newValue.length() > MAX_MES_LEN) {
							dialog.getEditor().setText(oldValue);
						}
					}
				});
				dialog.setTitle("Message broadcast");
				dialog.setHeaderText("Broadcast a message");
				dialog.setContentText("Message:");
				
				Optional<String> result = dialog.showAndWait();
				result.ifPresent(message -> {
					sHandler.broadcast(message);
					chat.appendTextLn(message);
				});
			}
		});
		
		serverServerMenu.getItems().add(broadcastItem);
		
		serverMenu.getMenus().addAll(serverFileMenu, serverServerMenu);
		
		switchMenu(homeMenu);
		
		homePane = new GridPane();
		homePane.setAlignment(Pos.CENTER);
		homePane.setVgap(10);
		
		Button bCreate = new Button("Host a server");
		bCreate.setMinHeight(40);
		bCreate.setMaxWidth(Double.MAX_VALUE);
		Button bConnect = new Button("Connect to an existing server");
		bConnect.setMinHeight(40);
		bConnect.setMaxWidth(Double.MAX_VALUE);
		
		RowConstraints rCon11 = new RowConstraints();
		rCon11.setPercentHeight(50);
		RowConstraints rCon12 = new RowConstraints();
		rCon12.setPercentHeight(50);
		homePane.getRowConstraints().addAll(rCon11, rCon12);
		
		ColumnConstraints cCon11 = new ColumnConstraints();
		cCon11.setFillWidth(true);
		cCon11.setPercentWidth(50);
		homePane.getColumnConstraints().addAll(cCon11);
		
		GridPane.setHalignment(bCreate, HPos.CENTER);
		GridPane.setValignment(bCreate, VPos.BOTTOM);
		homePane.add(bCreate, 0, 0);
		
		GridPane.setHalignment(bConnect, HPos.CENTER);
		GridPane.setValignment(bConnect, VPos.TOP);
		homePane.add(bConnect, 0, 1);
		
		conPane = new GridPane();
		conPane.setVgap(10);
		conPane.setHgap(10);
		conPane.setPadding(new Insets(10, 10, 10, 10));
		
		RowConstraints rCon21 = new RowConstraints();
		rCon21.setVgrow(Priority.ALWAYS);
		RowConstraints rCon22 = new RowConstraints(20);
		conPane.getRowConstraints().addAll(rCon21, rCon22);
		
		ColumnConstraints cCon21 = new ColumnConstraints();
		cCon21.setHgrow(Priority.ALWAYS);
		ColumnConstraints cCon22 = new ColumnConstraints(55);
		cCon22.setHalignment(HPos.RIGHT);
		ColumnConstraints cCon23 = new ColumnConstraints();
		cCon23.setPercentWidth(30);
		conPane.getColumnConstraints().addAll(cCon21, cCon22, cCon23);
		
		chat = new ChatOutputArea();
		chat.setEditable(false);
		chat.setWrapText(true);
		
		conPane.add(chat, 0, 0, 2, 1);
		
		TextField chatInput = new TextField();
		
		conPane.add(chatInput, 0, 1);
		
		Label lCharCount = new Label();
		lCharCount.setAlignment(Pos.CENTER_RIGHT);
		lCharCount.textProperty().bind(Bindings.concat(chatInput.textProperty().length(), "/" + MAX_MES_LEN));
		
		conPane.add(lCharCount, 1, 1);
		
		userList = new ListView<String>();
		
		conPane.add(userList, 2, 0);
		
		Label lUserCount = new Label();
		userCount = new SimpleIntegerProperty(0);
		userMax = new SimpleIntegerProperty(0);
		lUserCount.textProperty().bind(Bindings.concat("Users: ", userCount, "/", userMax));
		
		conPane.add(lUserCount, 2, 1);
		
		Scene primaryScene = new Scene(outerPane, 800, 600);
		switchPane(homePane);
		
		primaryStage.setScene(primaryScene);
		
		bCreate.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				Dialog<ServerInfo> dialog = new Dialog<>();
				dialog.setTitle("Host a server");
				dialog.setHeaderText("Host a server");
				
				GridPane dialogPane = new GridPane();
				dialogPane.setVgap(10);
				
				TextField maxUsersArea = new TextField();
				dialogPane.add(new Label("Max users: "), 0, 0);
				dialogPane.add(maxUsersArea, 1, 0);
				
				TextField usernameArea = new TextField();
				dialogPane.add(new Label("Username: "), 0, 1);
				dialogPane.add(usernameArea, 1, 1);
				
				dialog.getDialogPane().setContent(dialogPane);
				
				dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
				Node button = dialog.getDialogPane().lookupButton(ButtonType.OK);
				button.setDisable(true);
				boolean[] set = new boolean[2];

			    Tooltip tooltip = new Tooltip("Must be between 1 and 127 (inclusive)");
			    tooltip.setAutoHide(true);
				
				maxUsersArea.textProperty().addListener(new ChangeListener<String>() {
					
					@Override
					public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
						tooltip.hide();
						maxUsersArea.setTooltip(null);
						if(!newValue.matches("\\^d*$")) {
							maxUsersArea.setText(newValue.replaceAll("\\D+", ""));
						}
						try {
							int x = Integer.parseInt(maxUsersArea.getText());
							if(x > 127 || x < 1) {
								maxUsersArea.setText(oldValue);
								maxUsersArea.setTooltip(tooltip);
								Point2D p = maxUsersArea.localToScene(0.0, 0.0);
								tooltip.show(maxUsersArea, p.getX()
								        + maxUsersArea.getScene().getX() + maxUsersArea.getScene().getWindow().getX(), 
								        p.getY() + maxUsersArea.getScene().getY()
								        + maxUsersArea.getScene().getWindow().getY() + maxUsersArea.getHeight());
							}
						}
						catch(NumberFormatException e) {
							
						}
						set[0] = !maxUsersArea.getText().trim().equals("");
						button.setDisable(!set[0] || !set[1]);
					}
				});
				
				usernameArea.textProperty().addListener(new ChangeListener<String>() {
					
					@Override
					public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
						if(usernameArea.getText().length() > MAX_NAME_LEN) {
							usernameArea.setText(usernameArea.getText(0, MAX_NAME_LEN));
						}
						set[1] = !usernameArea.getText().trim().equals("");
						button.setDisable(!set[0] || !set[1]);
					}
				});
				
				dialog.setResultConverter(new Callback<ButtonType, ServerInfo>() {
					
					@Override
					public ServerInfo call(ButtonType param) {
						if(param == ButtonType.OK) {
							return new ServerInfo(Integer.parseInt(maxUsersArea.getText()), usernameArea.getText(), 0);
						}
						else
							return null;
					}
				});
				
				Optional<ServerInfo> result = dialog.showAndWait();
				
				result.ifPresent(serverInfo -> {
					serverInfo = result.get();
					userMax.set(serverInfo.getMaxUsers());
					ChatterServer server = new ChatterServer(
						serverInfo.getUsername(), 
						serverInfo.getMaxUsers(), 
						Chatter.this
					);
					if(server.host()) {
						handler = server;
						sHandler = server;
						addUser(serverInfo.getUsername());
						serverView();
					}
					else {
						Alert alert = new Alert(AlertType.ERROR);
						alert.setTitle("Unable to host a server");
						alert.setHeaderText("Unable to host a server");
						alert.showAndWait();
					}
				});
			}
		}); //TODO: close server on closing
		
		bConnect.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				Dialog<ServerInfo> dialog = new Dialog<>();
				dialog.setTitle("Connect to a server");
				dialog.setHeaderText("Connect to a server");
				
				GridPane dialogPane = new GridPane();
				dialogPane.setVgap(10);
				
				TextField addressArea = new TextField();
				dialogPane.add(new Label("Address: "), 0, 0);
				dialogPane.add(addressArea, 1, 0);
				
				TextField usernameArea = new TextField();
				dialogPane.add(new Label("Username: "), 0, 1);
				dialogPane.add(usernameArea, 1, 1);
				
				dialog.getDialogPane().setContent(dialogPane);
				
				dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
				Node button = dialog.getDialogPane().lookupButton(ButtonType.OK);
				button.setDisable(true);
				boolean[] set = new boolean[2];
				
				addressArea.textProperty().addListener(new ChangeListener<String>() {
					
					@Override
					public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
						set[0] = !addressArea.getText().trim().equals("");
						button.setDisable(!set[0] || !set[1]);
					}
				});
				
				usernameArea.textProperty().addListener(new ChangeListener<String>() {
					
					@Override
					public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
						if(usernameArea.getText().length() > MAX_NAME_LEN) {
							usernameArea.setText(usernameArea.getText(0, MAX_NAME_LEN));
						}
						set[1] = !usernameArea.getText().trim().equals("");
						button.setDisable(!set[0] || !set[1]);
					}
				});
				
				dialog.setResultConverter(new Callback<ButtonType, ServerInfo>() {
					
					@Override
					public ServerInfo call(ButtonType param) {
						if(param == ButtonType.OK) {
							return new ServerInfo(addressArea.getText(), usernameArea.getText(), 0);
						}
						else
							return null;
					}
				});
				
				Optional<ServerInfo> result = dialog.showAndWait();
				
				result.ifPresent(serverInfo -> {
					ChatterClientSocket socket = new ChatterClientSocket(serverInfo.getUsername(), Chatter.this);
					if(socket.connect(serverInfo.getAddress())) {
						handler = socket;
						clientView(serverInfo.getUsername());
					}
					else {
						Alert alert = new Alert(AlertType.ERROR);
						alert.setTitle("Unable to connect");
						alert.setHeaderText("Unable to connect");
						alert.setContentText("Unable to connect to " + serverInfo.getAddress());
						alert.showAndWait();
					}
				});
			}
		});
		
		chatInput.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				 handler.pushChatMessage(chatInput.getText());
				 chatInput.setText("");				
			}
		});
		
		chatInput.textProperty().addListener(new ChangeListener<String>() {
			
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if(chatInput.getText().length() > MAX_MES_LEN) {
					chatInput.setText(chatInput.getText(0, MAX_MES_LEN));
				}
			}
		});
		
		primaryStage.setOnCloseRequest((event) -> {
			tryClose();
		});
		
		primaryStage.show();
	}

	@Override
	public void handleTextMessage(TextMessage mes) {
		Platform.runLater(()->{ chat.appendMessage(mes); });
	}

	@Override
	public void handleRename(String oldName, String newName) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void handleGreeting(int maxUsers, String hostName) {
		Platform.runLater(()->{
			userMax.set(maxUsers);
			addUser(hostName);
		});
	}
	

	@Override
	public void handleUserListFragment(String[] list) {
		Platform.runLater(()->{
			for(String u : list) {
				if(u != null) {
					addUser(u);
				}
			}
		});
	}

	@Override
	public void handleUserJoin(String name) {
		Platform.runLater(()->{ addUser(name); });
		
	}

	@Override
	public void handleUserLeave(String name) {
		Platform.runLater(()->{ removeUser(name); });
		
	}
	
	@Override
	public void handleDisconnect() {
		handleDisconnect("...");
	}

	@Override
	public void handleDisconnect(String reason) {
		Platform.runLater(()->{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Disconnected");
			alert.setHeaderText("You were disconnected");
			alert.setContentText("Reason: " + reason);
			alert.showAndWait();
			resetView();
		});
	}

	@Override
	public void handleServerClose() {
		Platform.runLater(()->{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Disconnected");
			alert.setHeaderText("You were disconnected");
			alert.setContentText("The server has closed");
			alert.showAndWait();
			resetView();
		});			
	}

	@Override
	public void handleKick(String reason) {
		Platform.runLater(()->{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Disconnected");
			alert.setHeaderText("You were kicked");
			alert.setContentText((reason == null ? "" : "Reason: " + reason));
			alert.showAndWait();
			resetView();
		});	
	}
	
	@Override
	public void handleFull() {
		Platform.runLater(()->{
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Unable to connect");
			alert.setHeaderText("Unable to connect");
			alert.setContentText("Reason: server full");
			alert.showAndWait();
			resetView();
		});		
	}
	
	@Override
	public void handleEncryptionError(String error) {
		Platform.runLater(()->{
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Encryption error");
			alert.setHeaderText("Communication will not be encrypted");
			alert.setContentText(error);
			alert.showAndWait();
		});
	}

	public static void main(String[] args) {
		launch(args);
	}
}
