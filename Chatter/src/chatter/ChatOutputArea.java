package chatter;
import javafx.scene.control.TextArea;
import message.TextMessage;

public class ChatOutputArea extends TextArea {
	public void appendTextLn(String text) {
		if(getText().equals("")) {
			appendText(text);
		}
		else {
			appendText("\n" + text);
		}
		setScrollTop(Double.MAX_VALUE);
	}
	
	public void appendMessage(TextMessage message) {
		appendTextLn(message.getMessageText());
	}
}
