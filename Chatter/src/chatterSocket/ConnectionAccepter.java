package chatterSocket;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

class ConnectionAccepter implements Runnable {
	private Thread t;
	private ServerSocket ss;
	private Queue<Socket> queue;
	private Object server;
	
	ConnectionAccepter(Object server) {
		this.server = server;
		t = new Thread(this, "Connection accepter");
		t.setDaemon(true);
		queue = new ConcurrentLinkedQueue<Socket>();
	}
	
	@Override
	public void run() {
		try {
			ss.setSoTimeout(5000);
			while(!Thread.interrupted()) {
				try {
					queue.offer(ss.accept());
					synchronized(server) {
						server.notify();
					}
				}
				catch(SocketTimeoutException e) {
					
				}
			}
		}
		catch(IOException e) {
			Thread.currentThread().interrupt();
		}
	}
	
	void start() throws IOException{
		ss = new ServerSocket(ChatterSocket.PORT);
		t.start();
	}
	
	void stop() throws IOException{
		t.interrupt();
		ss.close();
	}
	
	Socket poll() {
		return queue.poll();
	}
}
