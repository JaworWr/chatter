package chatterSocket.crypto;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

import chatterSocket.ChatterSocketInputHandler;

public class ServerStreamEncrypter extends StreamEncrypter {

	private byte[] publKeyBytes;
	
	public ServerStreamEncrypter(ChatterSocketInputHandler handler, ServerStreamEncrypterKeyData keyData) {
		super(handler);
		this.publKeyBytes = keyData.getPublKeyBytes();
		this.priv = keyData.getCipher();
		if(this.publKeyBytes == null) {
			ciphersSet = false;
		}
	}

	@Override
	public void encrypt(InputStream in, OutputStream out) throws IOException {
		closeStreams();
		if(ciphersSet) {
			try {
				out.write(publKeyBytes);
				
				byte[] symKeyBytes = new byte[256];
				in.read(symKeyBytes);
				SecretKey symKey = (SecretKey) priv.unwrap(symKeyBytes, symString, Cipher.SECRET_KEY);
				
				symOut.init(Cipher.ENCRYPT_MODE, symKey);
				byte[] symIv = symOut.getIV();
				out.write(symIv);
				in.read(symIv);
				symIn.init(Cipher.DECRYPT_MODE, symKey, new IvParameterSpec(symIv));
				
				encrIn = new CipherInputStream(in, symIn);
				encrOut = new CipherOutputStream(out, symOut);
				streamsSet = true;
			}
			catch(NoSuchAlgorithmException e) {
				e.printStackTrace();
				handler.handleEncryptionError("One of the algorithms is unsupported.");
				ciphersSet = false;
				encrIn = in;
				encrOut = out;
			}
			catch(InvalidKeyException e) {
				e.printStackTrace();
				handler.handleEncryptionError("Invalid key");
				encrIn = in;
				encrOut = out;
			}
			catch(InvalidAlgorithmParameterException e) {
				e.printStackTrace();
				handler.handleEncryptionError("An error occurred during encryption initialization.");
				encrIn = in;
				encrOut = out;
			}
		}
		else {
			encrIn = in;
			encrOut = out;
		}
	}
}
