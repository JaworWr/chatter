package chatterSocket.crypto;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;

import chatterSocket.ChatterSocketInputHandler;

public abstract class StreamEncrypter {
	static final String symString = "AES";
	static final String privString = "RSA";
	static final String symFullString = "AES/CTR/NoPadding";
	static final String privFullString = "RSA/ECB/PKCS1Padding";
	
	protected InputStream encrIn;
	protected OutputStream encrOut;
	
	protected Cipher priv;
	protected Cipher symIn;
	protected Cipher symOut;
	
	protected boolean ciphersSet;
	protected boolean streamsSet;
	protected ChatterSocketInputHandler handler;
	
	protected StreamEncrypter(ChatterSocketInputHandler handler) {
		this.handler = handler;
		this.streamsSet = false;
		try {
			this.symIn = Cipher.getInstance(symFullString);
			this.symOut = Cipher.getInstance(symFullString);
			this.ciphersSet = true;
		}
		catch(NoSuchPaddingException | NoSuchAlgorithmException e) {
			e.printStackTrace();
			this.handler.handleEncryptionError("One of the algorithms is unsupported.");
			this.ciphersSet = false;
		}
	}
	
	public InputStream getInputStream() {
		return encrIn;
	}
	
	public OutputStream getOutputStream() {
		return encrOut;
	}
	
	public void closeStreams() throws IOException {
		if(streamsSet) {
			encrOut.close();
			encrIn.close();
		}
		streamsSet = false;
	}
	
	public abstract void encrypt(InputStream in, OutputStream out) throws IOException;
}
