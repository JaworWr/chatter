package chatterSocket.crypto;

import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;

import chatterSocket.ChatterSocketInputHandler;

public class ServerStreamEncrypterKeyData {
	
	private KeyPair keyPair;
	private byte[] publKeyBytes;
	private Cipher cipher;
	
	public ServerStreamEncrypterKeyData(ChatterSocketInputHandler handler) {
		try {
			keyPair = KeyPairGenerator.getInstance(StreamEncrypter.privString).generateKeyPair();
			publKeyBytes = keyPair.getPublic().getEncoded();
			cipher = Cipher.getInstance(StreamEncrypter.privFullString);
			cipher.init(Cipher.UNWRAP_MODE, keyPair.getPrivate());
		}
		catch(NoSuchAlgorithmException | NoSuchPaddingException e) {
			e.printStackTrace();
			handler.handleEncryptionError("One of the algorithms is unsupported.");
			cipher = null;
			publKeyBytes = null;
		}
		catch(InvalidKeyException e) {
			e.printStackTrace();
			handler.handleEncryptionError("Invalid key.");
			cipher = null;
			publKeyBytes = null;
		}
	}
	
	byte[] getPublKeyBytes() {
		return publKeyBytes;
	}
	
	Cipher getCipher() {
		return cipher;
	}
}
