package chatterSocket.crypto;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

import chatterSocket.ChatterSocketInputHandler;

public class ClientStreamEncrypter extends StreamEncrypter {

	public ClientStreamEncrypter(ChatterSocketInputHandler handler) {
		super(handler);
		try {
			this.priv = Cipher.getInstance(privFullString);
		}
		catch(NoSuchPaddingException | NoSuchAlgorithmException e) {
			e.printStackTrace();
			this.handler.handleEncryptionError("One of the algorithms is unsupported.");
			this.ciphersSet = false;
		}
	}

	@Override
	public void encrypt(InputStream in, OutputStream out) throws IOException {
		closeStreams();
		if(ciphersSet) {
			try {
				SecretKey symKey = KeyGenerator.getInstance(symString).generateKey();
				
				byte[] publKeyBytes = new byte[2048];
				in.read(publKeyBytes);
				PublicKey publKey = KeyFactory.getInstance(privString).generatePublic(
						new X509EncodedKeySpec(publKeyBytes));
				priv.init(Cipher.WRAP_MODE, publKey);
				
				byte[] symKeyBytes = priv.wrap(symKey);
				out.write(symKeyBytes);
				
				symOut.init(Cipher.ENCRYPT_MODE, symKey);
				byte[] symIv = symOut.getIV();
				out.write(symIv);
				in.read(symIv);
				symIn.init(Cipher.DECRYPT_MODE, symKey, new IvParameterSpec(symIv));
				
				encrIn = new CipherInputStream(in, symIn);
				encrOut = new CipherOutputStream(out, symOut);
				streamsSet = true;
			}
			catch(NoSuchAlgorithmException e) {
				e.printStackTrace();
				handler.handleEncryptionError("One of the ciphers is unsupported.");
				ciphersSet = false;
				encrIn = in;
				encrOut = out;
			}
			catch(InvalidKeyException | InvalidKeySpecException e) {
				e.printStackTrace();
				handler.handleEncryptionError("Invalid key");
				encrIn = in;
				encrOut = out;
			}
			catch(InvalidAlgorithmParameterException | IllegalBlockSizeException e) {
				e.printStackTrace();
				handler.handleEncryptionError("An error occurred during encryption initialization.");
				encrIn = in;
				encrOut = out;
			}
		}
		else {
			encrIn = in;
			encrOut = out;
		}
	}

}
