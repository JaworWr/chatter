package chatterSocket;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketException;

import chatterSocket.crypto.ClientStreamEncrypter;
import chatterSocket.crypto.StreamEncrypter;
import message.*;
import message.JoinLeaveMessage.Reason;

public class ChatterClientSocket extends ChatterSocket implements ChatterSocketOutputHandler {
	private String username;
	private boolean disconnectHandled;
	private StreamEncrypter encrypter;
	
	public ChatterClientSocket(String username, ChatterSocketInputHandler handler) {
		super(handler);
		this.username = username;
		this.encrypter = new ClientStreamEncrypter(handler);
	}

	@Override
	public void run() {
		try {
			s.setSoTimeout(50);
			sw.push(new GreetingMessage(username));
			Message mes = sl.poll();
			while(mes == null || mes.getType() != Message.Type.GREETING) {
				mes = sl.poll();
			}
			int count = ((GreetingMessage)mes).getCount();
			if(count < 0) {
				handler.handleFull();
				stop();
			}
			else {
				handler.handleGreeting(((GreetingMessage)mes).getMaxUsers(), ((GreetingMessage)mes).getName());
				while(count > 1) {
					mes = sl.poll();
					while(mes == null || mes.getType() != Message.Type.USER_LIST) {
						mes = sl.poll();
					}
					handler.handleUserListFragment(((UserListMessage)mes).getList());
					count -= ((UserListMessage)mes).getCount();
				}
				handler.handleUserJoin(username);
				disconnectHandled = false;
				while(sl.isOk()) {
					mes = sl.poll();
					if(mes != null) {
						if(mes.isTextMessage()) {
							handler.handleTextMessage((TextMessage)mes);
						}
						switch(mes.getType()) {
							case JOIN_LEAVE:
								if(((JoinLeaveMessage)mes).getReason() == Reason.JOINED)
									handler.handleUserJoin(((JoinLeaveMessage)mes).getName());
								else
									handler.handleUserLeave(((JoinLeaveMessage)mes).getName());
								break;
							case RENAME:
								handler.handleRename(((RenameMessage)mes).getOldName(),
										((RenameMessage)mes).getNewName());
								break;
							case SERVER:
								switch(((ServerMessage)mes).getStatement()) {
									case CLOSED:
										handler.handleServerClose();
										disconnectHandled = true;
										break;
									case KICKED:
										handler.handleKick(((ServerMessage)mes).getMessage());
										disconnectHandled = true;
										break;
									case DUPLICATE:
										handler.handleDisconnect("This name is already in use");
										disconnectHandled = true;
									default:;
								}
							default:;
						}
					}
				}
				throw new DisconnectException();
			}
		}
		catch(SocketException e) {
			e.printStackTrace();
		}
		catch(DisconnectException e) {
			if(!disconnectHandled)
				handler.handleDisconnect();
			closeSocket();
		}
	}
	
	public boolean connect(String address) { //TODO: throw exception describing failure cause
		try {
			if(!s.isConnected()) {
				s.connect(new InetSocketAddress(address, PORT), 1000);
				encrypter.encrypt(s.getInputStream(), s.getOutputStream());
				sw = new StreamWriter(encrypter.getOutputStream());
				sl = new StreamListener(encrypter.getInputStream(), this);
				t.start();
				return true;
			}
			return false;
		}
		catch(IOException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	@Override
	public void pushChatMessage(String message) {
		try {
			ChatMessage mes = new ChatMessage(username, message);
			sw.push(mes);
			handler.handleTextMessage(mes);			
		}
		catch (DisconnectException e) {
			closeSocket();
			handler.handleDisconnect();
		}
	}
	
	@Override
	public void rename(String newName) {
		try {
			RenameMessage mes = new RenameMessage(username, newName);
			sw.push(mes);
			username = newName;
			handler.handleTextMessage(mes);
		}
		catch (DisconnectException e) {
			closeSocket();
			handler.handleDisconnect();
		}
	}
	
	@Override
	public void stop() {
		disconnectHandled = true;
		sw.pushAndLock(new JoinLeaveMessage(username, JoinLeaveMessage.Reason.LEFT));
		closeSocket();
	}
	
	@Override
	protected void closeSocket() {
		try {
			sl.stop();
			sw.stop();
			t.interrupt();
			encrypter.closeStreams();
			s.close();
		}
		catch(IOException e) {
			
		}
	}
}
