package chatterSocket;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicReference;

import chatterSocket.crypto.ServerStreamEncrypter;
import chatterSocket.crypto.ServerStreamEncrypterKeyData;
import chatterSocket.crypto.StreamEncrypter;
import message.*;
import message.JoinLeaveMessage.Reason;
import message.ServerMessage.Statement;

public class ChatterServerSocket extends ChatterSocket {
	private static class DuplicateNamesException extends Exception {

		/**
		 * 
		 */
		private static final long serialVersionUID = 7L;
		
	}
	
	private static enum State {
		NOT_CONNECTED, CONNECTED, CLOSING, KICK, USER_DISCONNECT, CLOSED
	}
	
	private String name;
	private ChatterServer server;
	
	private StreamEncrypter encrypter;
	
	private AtomicReference<State> state;
	
	public ChatterServerSocket(ChatterServer server, ChatterSocketInputHandler handler, Socket socket, ServerStreamEncrypterKeyData keyData) {
		super(handler, socket);
		this.name = "";
		this.server = server;
		this.state = new AtomicReference<>(State.NOT_CONNECTED);
		this.encrypter = new ServerStreamEncrypter(handler, keyData);
	}
	
	@Override
	public void run() {
		try{
			Collection<String> usernames = server.getUsernames();
			s.setSoTimeout(500);
			sw.push(new GreetingMessage((byte)server.getUserCount(), (byte)server.getMaxUsers(), server.getHostName()));
			for(int sent = 0; sent < server.getUserCount(); sent += UserListMessage.MAX_COUNT) {
				sw.push(new UserListMessage((byte)Math.min(UserListMessage.MAX_COUNT, server.getUserCount() - sent),
						Arrays.copyOfRange(usernames.toArray(new String[usernames.size()]),
								sent, sent + UserListMessage.MAX_COUNT)));
			}
			Message mes = sl.poll();
			while(mes == null || mes.getType() != Message.Type.GREETING) {
				mes = sl.poll();
			}
			name = ((GreetingMessage)mes).getName();
			if(usernames.contains(name)) {
				throw new DuplicateNamesException();
			}
			mes = new JoinLeaveMessage(name, Reason.JOINED);
			handler.handleTextMessage((TextMessage)mes);
			handler.handleUserJoin(name);
			server.forward(this, mes);
			while(state.get() == State.CONNECTED && sl.isOk()) {
				mes = sl.poll();
				if(mes != null) {
					server.forward(this, mes);
					if(mes.isTextMessage()) {
						handler.handleTextMessage((TextMessage)mes);
					}
					switch(mes.getType()) {
						case JOIN_LEAVE: //can only be received if the user is disconnecting
							server.removeSocket(this, false, true);
							state.set(State.USER_DISCONNECT);
							closeSocket();
							break;
						case RENAME:
							handler.handleRename(name, ((RenameMessage)mes).getNewName());
							name = ((RenameMessage)mes).getNewName();
							break;
						default:;
					}
				}
			}
			if(state.get() != State.USER_DISCONNECT) {
				if(state.get() == State.CLOSING) {
					sw.pushAndLock(new ServerMessage(Statement.CLOSED));
				}
				throw new DisconnectException();
			}
		}
		catch(SocketException e) {
			e.printStackTrace();
		}
		catch(DuplicateNamesException e) {
			server.removeSocket(this, false, false);
			sw.pushAndLock(new ServerMessage(Statement.DUPLICATE));
			state.set(State.CLOSED);
			closeSocket();
		}
		catch(DisconnectException e) {
			state.set(State.CLOSED);
			server.removeSocket(this, true, true);
			closeSocket();
		}
	}
	
	public void pushMessage(Message mes) {
		try {
			sw.push(mes);
		}
		catch(DisconnectException e) {
			close();
		}
	}
	
	public void close() {
		state.set(State.CLOSING);
		try {
			t.join();
		}
		catch(InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}
	public void kick() {
		state.set(State.KICK);
		try {
			t.join();
		}
		catch(InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}
	
	public synchronized String getName() {
		return name;
	}
	
	public boolean isConnected() {
		return state.get() == State.CONNECTED;
	}

	public boolean connect() { //TODO: throw exception describing failure cause
		try {
			if(state.get() == State.NOT_CONNECTED) {
				state.set(State.CONNECTED);
				encrypter.encrypt(s.getInputStream(), s.getOutputStream());
				sw = new StreamWriter(encrypter.getOutputStream());
				sl = new StreamListener(encrypter.getInputStream(), this);
				t.start();
				return true;
			}
			return false;
		}
		catch(IOException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	@Override
	protected void closeSocket() {
		try {
			sl.stop();
			sw.stop();
			t.interrupt();
			encrypter.closeStreams();
			s.close();
		}
		catch(IOException e) {
			
		}
	}
	
}
