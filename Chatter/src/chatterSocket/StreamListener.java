package chatterSocket;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.SocketTimeoutException;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;

import message.Message;

class StreamListener implements Runnable {
	
	private Thread t;
	private SynchronousQueue<Message> queue;
	private ChatterStreamStatus status;
	private ObjectInputStream stream;
	private ChatterSocket socket;
	
	public StreamListener(InputStream stream, ChatterSocket socket) throws IOException {
		this.t = new Thread(this, "Stream listener");
		this.queue = new SynchronousQueue<Message>();
		this.status = ChatterStreamStatus.OK;
		this.stream = new ObjectInputStream(stream);
		this.socket = socket;
		
		t.setDaemon(true);
		t.start();
	}

	@Override
	public void run() {
		while(!Thread.interrupted() && isOk()) {
			try {
				Message mes;
				Object obj;
				synchronized(stream) {
					obj = stream.readObject();
					mes = (Message) obj;
				}
				queue.put(mes);
				synchronized(socket) { // TODO: deadlock here ;(
					socket.notify();
				}
			}
			catch(InterruptedException e) {
				Thread.currentThread().interrupt();
			}
			catch(EOFException e) {
				synchronized(this) {
					status = ChatterStreamStatus.DISCONNECTED;
				}
				Thread.currentThread().interrupt();
			}
			catch(SocketTimeoutException e) {
				
			}
			catch(IOException e) {
				e.printStackTrace();
				synchronized(this) {
					status = ChatterStreamStatus.IO_ERROR;
				}
				synchronized(socket) {
					socket.notify();
				}
				Thread.currentThread().interrupt();
			} 
			catch(ClassNotFoundException e) {
				e.printStackTrace();
				synchronized(this) {
					status = ChatterStreamStatus.IO_ERROR;
				}
				synchronized(socket) {
					socket.notify();
				}
				Thread.currentThread().interrupt();
			}	
		}
	}
	
	Message poll() throws DisconnectException{
		while(isOk()) {
			try {
				return queue.poll(100, TimeUnit.MILLISECONDS);
			}
			catch(InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		}
		throw new DisconnectException(status);
	}
	
	void stop() {
		setStatus(ChatterStreamStatus.DISCONNECTED);
		t.interrupt();
		try {
			t.join();
		}
		catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}	
	}
	
	synchronized boolean isOk() {
		return status == ChatterStreamStatus.OK;
	}
	
	synchronized private void setStatus(ChatterStreamStatus status) {
		this.status = status;
	}
	
	synchronized void setStream(InputStream stream) throws IOException {
		this.stream = new ObjectInputStream(stream);
	}

	public ChatterStreamStatus getStatus() {
		return status;
	}

}
