package chatterSocket;

public interface ChatterSocketOutputHandler {
	public void pushChatMessage(String message);
	public void rename(String newName);
	public void stop();
}
