package chatterSocket;

import message.TextMessage;

public interface ChatterSocketInputHandler {
	public void handleTextMessage(TextMessage mes);
	public void handleRename(String oldName, String newName);
	public void handleGreeting(int maxUsers, String hostName);
	public void handleUserListFragment(String[] list);
	public void handleUserJoin(String name);
	public void handleUserLeave(String name);
	public void handleDisconnect();
	public void handleDisconnect(String reason);
	public void handleServerClose();
	public void handleKick(String reason);
	public void handleFull();
	public void handleEncryptionError(String error);
}
