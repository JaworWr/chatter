package chatterSocket;

import java.io.IOException;
import java.net.Socket;

import message.*;

abstract public class ChatterSocket implements Runnable {
	protected StreamListener sl;
	protected StreamWriter sw;
	protected Socket s;
	protected Thread t;
	protected ChatterSocketInputHandler handler;
	
	public static int PORT = 6666;
	
	public ChatterSocket(ChatterSocketInputHandler handler) {
		this.s = new Socket();
		this.t = new Thread(this, "Socket thread") ;
		this.handler = handler;
		t.setDaemon(true);
	}	

	public ChatterSocket(ChatterSocketInputHandler handler, Socket socket) {
		this.s = socket;
		this.t = new Thread(this, "Socket thread") ;
		this.handler = handler;
		t.setDaemon(true);
	}
	
	protected abstract void closeSocket();
	
	static void rejectConnection(Message mes, Socket s) {
		try {
			StreamWriter.sendSingleMessage(mes, s.getOutputStream());
			s.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
