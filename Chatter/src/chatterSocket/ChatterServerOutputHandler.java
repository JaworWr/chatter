package chatterSocket;

public interface ChatterServerOutputHandler {
	public void kick(String username, String reason);
	public void broadcast(String message);
}
