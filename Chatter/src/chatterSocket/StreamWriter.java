package chatterSocket;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;

import message.Message;

class StreamWriter implements Runnable {

	private Thread t;
	private Queue<Message> queue;
	private ChatterStreamStatus status;
	private ObjectOutputStream stream;
	private AtomicBoolean unlocked;
	
	public StreamWriter(OutputStream stream) throws IOException {
		this.t = new Thread(this, "Stream writer");
		this.queue = new ConcurrentLinkedQueue<Message>();
		this.status = ChatterStreamStatus.OK;
		this.stream = new ObjectOutputStream(stream);
		this.stream.flush();
		this.unlocked = new AtomicBoolean(true);
		
		t.setDaemon(true);
		t.start();
	}
	
	@Override
	public void run() {
		while(!Thread.interrupted()) {
			try {
				Message mes = null;
				mes = queue.poll();
				if(mes == null) {
					synchronized(this) {
						wait();
					}
				}
				else {
					stream.writeObject(mes);
				}			
			}
			catch(InterruptedException e) {
				Thread.currentThread().interrupt();
			}
			catch(IOException e) {
				synchronized(this) {
					status = ChatterStreamStatus.IO_ERROR;
					Thread.currentThread().interrupt();
				}
			}
		}

	}
	
	void push(Message mes) throws DisconnectException {
		if(unlocked.get() && t.isAlive()) {
			queue.offer(mes);
			synchronized(this) {
				notify();
			}
		}
		else {
			throw new DisconnectException(status);
		}
	}
	
	void pushAndLock(Message mes) {
		if(unlocked.get() && t.isAlive()) {
			unlocked.set(true);
			queue.offer(mes);
			synchronized(this) {
				notify();
			}
		}		
	}
	
	void stop() {
		t.interrupt();
		try {
			t.join();
		}
		catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}	
	}
	
	synchronized boolean isOk() {
		return status == ChatterStreamStatus.OK;
	}
	
	synchronized void setStream(OutputStream stream) throws IOException {
		this.stream = new ObjectOutputStream(stream);
	}
	
	synchronized boolean isEmpty() {
		return queue.isEmpty();
	}
	
	void waitUntilEmpty() {
		while(t.isAlive() && !isEmpty()) {
			Thread.yield();
		}
	}
	
	static void sendSingleMessage(Message mes, OutputStream stream) {
		try {
			new ObjectOutputStream(stream).writeObject(mes);
		}
		catch(IOException e) {
			e.printStackTrace();
		}
	}

}
