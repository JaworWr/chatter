package chatterSocket;

enum ChatterStreamStatus {
	OK, DISCONNECTED, IO_ERROR, DATA_ERROR
}
