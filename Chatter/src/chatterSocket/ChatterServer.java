package chatterSocket;

import java.io.IOException;
import java.net.Socket;
import java.util.Collection;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import chatterSocket.crypto.ServerStreamEncrypterKeyData;
import message.*;
import message.JoinLeaveMessage.Reason;
import message.ServerMessage.Statement;

public class ChatterServer implements Runnable, ChatterSocketOutputHandler,	ChatterServerOutputHandler {
	private Thread t;
	private ConnectionAccepter accepter;
	private ChatterSocketInputHandler handler;
	private int maxConnections;
	private AtomicInteger users;
	private ServerStreamEncrypterKeyData keyData;
	
	private Collection<ChatterServerSocket> sockets;
	private String username;
	
	public ChatterServer(String username, int maxCount, ChatterSocketInputHandler handler) {
		this.sockets = new CopyOnWriteArrayList<ChatterServerSocket>();
		this.accepter = new ConnectionAccepter(this);
		this.t= new Thread(this, "Server thread");
		this.t.setDaemon(true);
		this.handler = handler;
		this.username = username;
		this.maxConnections = maxCount;
		this.users = new AtomicInteger(1);
		this.keyData = new ServerStreamEncrypterKeyData(handler);
	}
	
	@Override
	public void run() {
		try {
			Socket s;
			ChatterServerSocket socket;
			while(!Thread.interrupted()) {
				s = accepter.poll();
				if(s == null) {
					synchronized(this) {
						wait();
					}
				}
				else {
					if(users.get() >= maxConnections) {
						ChatterSocket.rejectConnection(new GreetingMessage((byte) -1, (byte)maxConnections, null), s);
					}
					else {
						socket = new ChatterServerSocket(this, handler, s, keyData);
						if(socket.connect()) {
							sockets.add(socket);
							users.incrementAndGet();
						}
					}
				}
			}
		}
		catch(InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}
	public boolean host() {
		try {
			accepter.start();
			t.start();
			return true;
		}
		catch(IOException e) {
			return false;
		}
	}

	@Override
	public void pushChatMessage(String message) {
		ChatMessage mes = new ChatMessage(username, message);
		forward(null, mes);
		handler.handleTextMessage(mes);
	}

	@Override
	public void rename(String newName) {
		RenameMessage mes = new RenameMessage(username, newName);
		forward(null, mes);
		username = newName;
		handler.handleTextMessage(mes);
	}

	@Override
	public void stop() {
		try {
			accepter.stop();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		t.interrupt();
		synchronized(this) {
			notify();
		}
		for(ChatterServerSocket s : sockets) {
			s.close();
		}
	}
	
	public void removeSocket(ChatterServerSocket socket, boolean lost, boolean remove) {
		sockets.remove(socket);
		JoinLeaveMessage mes;
		if(lost) {
			mes = new JoinLeaveMessage(socket.getName(), Reason.LOST);
			handler.handleTextMessage(mes);
			forward(socket, mes);
		}
		if(remove) {
			handler.handleUserLeave(socket.getName());
		}

		users.decrementAndGet();
	}
	
	public void forward(ChatterServerSocket sender, Message mes) {
		for(ChatterServerSocket s : sockets) {
			if(s != sender) {
				s.pushMessage(mes);
			}
		}
	}

	@Override
	public void kick(String username, String reason) {
		TextMessage mes = new JoinLeaveMessage(username, Reason.KICKED);
		for(ChatterServerSocket s : sockets) {
			if(s.getName().equals(username)) {
				s.pushMessage(new ServerMessage(Statement.KICKED, reason));
				s.kick();
			}
			else {
				s.pushMessage(mes);
			}
		}
		handler.handleTextMessage(mes);
	}

	@Override
	public void broadcast(String message) {
		forward(null, new BroadcastMessage(message));
	}
	
	public synchronized int getUserCount() {
		return sockets.size();
	}
	
	public synchronized int getMaxUsers() {
		return maxConnections;
	}
	
	public synchronized Collection<String> getUsernames() {
		return sockets.stream()
			.map(ChatterServerSocket::getName)
			.filter(s -> s.length() > 0)
			.collect(Collectors.toList());
	}
	
	public String getHostName() {
		return username;
	}
}
