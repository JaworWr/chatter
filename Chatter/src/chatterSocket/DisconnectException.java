package chatterSocket;

public class DisconnectException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private ChatterStreamStatus status;
	
	public DisconnectException() {
		this.status = null;
	}
	
	public DisconnectException(ChatterStreamStatus status) {
		this.status = status;
	}
	
	public ChatterStreamStatus getStatus() {
		return status;
	}
	
	@Override
	public String getMessage() {
		return "Connection error. Socket status: " + (status == null ? "unknown" : status.toString());
	}
}
